# sdraw

simple paint app

- text support
- rectangle and line tool
- image import/export

# installation

### building from source

    git clone https://codeberg.org/sleepntsheep/sdraw
    cd sdraw
    autoreconf -i
    ./configure
    sudo make install

### arch user repository

    yay -S sdraw # replace yay with your aur helper

[demo](https://www.youtube.com/watch?v=nmoqKl8hVA0)

